using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestControlAnim : MonoBehaviour
{
    Animator _animator;

    public Animation _animation;
    // Start is called before the first frame update
    void Start()
    {
        _animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            _animator.SetTrigger("CheckJump");
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            _animator.SetBool("CheckTalk", true);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            _animator.SetBool("CheckTalk", false);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            _animation.Play("Happy");
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            _animation.Stop("Happy");
        }
    }
}
