Space Food Pack 1.0

From Space Bacon to freeze dried Cherry pie - this extremely affordable Asset Pack contains all the food your Space Explorers need!

Includes 15 items and 25 variations. Unity 4 & Unity 5 compatible! The models are between 6-585 polygons. The package come with 2048x2048 and 1024x1024 atlased textures, both diffuse and normal maps. There are also prefabs for all the items in the different texture resolutions to make life easier for you.

This package is updated every now and then and it's price will go up, but everyone who already owns the package will of course get all updates for free.

Items (Polygons):
- Milk (585P)
- Orange juice (585P)
- Packet Meatloaf (90P)
- Packet Cherry pie (90P)
- Packet Lasagne (90P)
- Space Bacon (272P)
- Space bar (118P)
- Space bars (6P)
- Space Cheese (272P)
- Space Pudding (272P)
- Tube of Tuna (301P)
- Tube of Turkey (301P)
- Tube of Teriyaki (301P)
- Water (585P)
- Whiskey (585P)

Feel free to contact the asset publisher if you have suggestions or complaints :) ! We offer technical support to anyone who has bought this pack via the Unity Asset Store.

E-mail: veera@nekobolt.net
Website: www.nekobolt.net