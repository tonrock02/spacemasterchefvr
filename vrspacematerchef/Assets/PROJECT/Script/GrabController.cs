using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabController : MonoBehaviour
{
    public List<GrabObject> _grabObject = new List<GrabObject>();
    public GrabObject _currentGrabObject;
    public bool grab;
    public bool hover;

    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    protected float m_prevFlex;

    [SerializeField]
    public OVRInput.Controller m_controller;
    Vector3 old_pointer;
    Vector3 _currentPointer;
    float _currentSpeed;
    Vector3 _direction;
    Vector3 _currentEuler;
    Vector3 old_euler;
    Vector3 _speedEuler;
    public Transform parent;
    private void Awake()
    {
        parent = transform.Find("parent").transform;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float prevFlex = m_prevFlex;
        // Update values from inputs
        m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

        CheckForGrabOrRelease(prevFlex);

        if (_currentGrabObject)
        {
            var pointer = transform.position;
            _currentPointer = Vector3.Lerp(_currentPointer, pointer, 20 * Time.deltaTime);

            var euler = transform.eulerAngles;
            _currentEuler = Vector3.Lerp(_currentEuler, euler,10 *Time.deltaTime);

            _speedEuler = (_currentEuler - old_euler).normalized / Time.deltaTime;

            var distance = (_currentPointer - old_pointer).magnitude;
            _direction = (_currentPointer - old_pointer).normalized;

            var speed = distance / Time.deltaTime;
            _currentSpeed = Mathf.Lerp(_currentSpeed, speed, Time.deltaTime);

            old_pointer = _currentPointer;
            old_euler = _currentEuler;
        }
    }

    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
        }

        GrabMove();
    }

    public void GrabBegin()
    {
        if (_grabObject.Count <= 0)
            return;
        //Debug.Log(transform.gameObject.name+ " Grab Start");
        if (!grab)
        {
            //_grabObject[0].gameObject.transform.parent = gameObject.transform;
            //_grabObject[0]._rb.isKinematic = true;
            //comment****

            _currentGrabObject = _grabObject[0];
            Debug.Log("Before Parent"+_currentGrabObject.transform.localScale); 
            _currentGrabObject.transform.parent = parent;
            //***
            Debug.Log("After Parent" + _currentGrabObject.transform.localScale);
            _currentGrabObject._rb.isKinematic = true;
            _currentGrabObject.transform.position = transform.position;
            //_currentGrabObject.transform.rotation = transform.rotation;
            //Debug.Log("Grab Begin");
            _currentGrabObject.GetComponent<Item>().hover = true;
            SoundManager.Instance._sounds[6].PlaySound();
        }
        grab = true;
    }

    public void GrabMove()
    {
        if (grab)
        {
            
            //_currentGrabObject.transform.position = transform.position;
            //_currentGrabObject.transform.rotation = transform.rotation;
        }
    }

    public void GrabEnd()
    {
        if (grab)
        {
            if (transform.childCount > 0)
            {
                foreach (Transform item in parent.transform)
                {
                    //item.GetComponent<GrabObject>()._rb.isKinematic = false;
                    //Debug.Log(" Grab End Angular Velocity:" + _currentGrabObject._rb.angularVelocity);
                    _currentGrabObject._rb.isKinematic = false;
                    _currentGrabObject._rb.AddForce(_direction*_currentSpeed*20);
                    _currentGrabObject._rb.AddTorque(_speedEuler*0.1f);
                    //Debug.Log(transform.gameObject.name + " Grab End : "+_speedEuler+" Angular "+_currentGrabObject._rb.angularVelocity);
                    item.transform.parent = null;
                    //item.parent = null;
                    item.transform.localScale = item.GetComponent<Item>()._startScale;
                    _currentGrabObject = null;
                }
            }
            grab = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GrabObject obj = other.GetComponent<GrabObject>();
        if (obj == null)
            return;
        if (other.gameObject.tag == "Item" && !_grabObject.Contains(obj))
        {
            hover = true;
            _grabObject.Add(obj);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GrabObject obj = other.GetComponent<GrabObject>();
        if (obj == null)
            return;
        
        if (_grabObject.Contains(obj)) 
        {
            //Debug.Log("Exit "+other.gameObject.name);
            _grabObject.Remove(obj);
            _grabObject.Clear();
            hover = false;
        }
    }

    public void FindNearestObject()
    {

    }

}
