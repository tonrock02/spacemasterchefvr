using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Supply : MonoBehaviour
{
    public GameObject _prefabSupply;
    bool hover=false;
    public Outline _line;
    float _lastTimePress;
    public float _cooldownPress=0.25f;
    public float _force= 0.5f;

    protected float m_prevFlex;

    float grabBegin = 0.55f;
    float grabEnd = 0.35f;

    private GameObject _spawnPoint;
    private void Awake()
    {
        _line = transform.GetComponent<Outline>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //_spawnPoint = GameObject.Find("Enviroment").transform.Find("SpawnPoint").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ActiveSupply();
        }

        if (hover)
        {
            _lastTimePress += Time.deltaTime;
            float prevFlex = m_prevFlex;
            //Debug.Log(OVRInput.Controller.RHand);
            m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger,OVRInput.Controller.RTouch);
            CheckForGrabOrRelease(prevFlex);
        }
    }

    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            //Start
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            Debug.Log("Grab Supply");
            if (CanAcitve())
            {
                _lastTimePress = 0;
                ActiveSupply();
            }
            SoundManager.Instance._sounds[5].PlaySound();
            //End
        }
    }

    public void ActiveSupply()
    {
        Debug.Log("Active");
        Transform spawnPoint = _spawnPoint == null ? transform : _spawnPoint.transform;

        Item supply = Instantiate(_prefabSupply, spawnPoint.position, Quaternion.identity).GetComponent<Item>();
        supply._rb.AddForce(transform.forward*_force,ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            hover = true;
            _line.enabled = true;
            SoundManager.Instance._sounds[2].PlaySound();
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            hover = false;
            _lastTimePress = 0;
            _line.enabled = false;
        }
    }

    public bool CanAcitve()
    {
        bool can = false;
        if (_lastTimePress > _cooldownPress)
        {
            can = true;
        }

        if(_lastTimePress < _cooldownPress)
        {
            can = false;
        }
            
        return can;
    }
}
