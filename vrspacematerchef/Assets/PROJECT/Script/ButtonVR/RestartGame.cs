using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnButtonPress(string function)
    {
        if (function == "restart game")
        {
            StartCoroutine(Restart());
        }
    }

    IEnumerator Restart()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Gameplay_new");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
