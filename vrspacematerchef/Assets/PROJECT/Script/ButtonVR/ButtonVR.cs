using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonVR : MonoBehaviour
{
    public Outline _line;
    public bool hover = false;

    public GrabController _grab;

    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    protected float m_prevFlex;

    public OVRInput.Controller m_controller;
    public Station _station;
    public CraftContainer _craft;
    public ResultContainer _result;
    public string _function;
    public RestartGame _restartGame;
 
    //public OpenInfo _openInfo;
    public ExitInfo _exitInfo;

    private void Update()
    {
        if (hover)
        {
            float prevFlex = m_prevFlex;
            // Update values from inputs
            m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

            CheckForGrabOrRelease(prevFlex);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _grab = other.GetComponent<GrabController>();
            if (_grab == null)
                return;

            /*if (_station._currentItem == null)
                return;*/

            m_controller = _grab.m_controller;

            if(_line!=null)
                _line.enabled = true;

            hover = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            if(_line!=null)
                _line.enabled = false;

            hover = false;
            _grab = null;
        }
    }

    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
        }
    }

    public void GrabBegin()
    {

    }

    public void GrabEnd()
    {
        if(_station)
        _station.OnButtonPress(_function);

        if(_craft)
        _craft.OnButtonPress(_function);

        if (_result)
        _result.OnButtonPress(_function);

        // if (_openInfo)
        // _openInfo.OnButtonPress(_function);

        if (_exitInfo)
        _exitInfo.OnButtonPress(_function);

        if (_restartGame)
            _restartGame.OnButtonPress(_function);
    }
}
