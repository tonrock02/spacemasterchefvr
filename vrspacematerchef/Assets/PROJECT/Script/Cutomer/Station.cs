using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Station : MonoBehaviour
{
    public TextMeshProUGUI _name;
    public Animator RedRobotAnim;
    public Animator SmallRobotAnim;
    [HideInInspector]
    public OrderUI _orderUI;
    [HideInInspector]
    public GameObject _water;
    [HideInInspector]
    public GameObject _wave;

    public Transform _pointer;
    public Requirement _currentRequirement;

    public RequirementManager _manager;

    public Item _currentItem;
    private GrabController _currentGrab;

    //private GameObject _wrong,correct;

    public List<Item> _listItem = new List<Item>();
    [HideInInspector]
    public List<Order.OrderClient> _tempListOrder = new List<Order.OrderClient>();

    public GameObject _paticle;

    [HideInInspector]
    public TextMeshProUGUI _scoreText, _timerText, _sumScoreText, _numClientText;

    private GameObject _center;
    private GameObject _mainDishPoint, _drinkPoint, _sideDishPoint;
    public GameObject _CanvasResult;
    public DishContainer _dishContainer;
    [HideInInspector]
    public TypeContainerData _typeContainer;
    [HideInInspector]
    private CraftContainer _craft;
    private float m_sumScore;
    public float _sumScore
    {
        get
        {
            return m_sumScore;
        }
        set
        {
            _sumScoreText.text = value.ToString();
            m_sumScore = value;
        }
    }

    public float _time=300;
    public float _maxTime = 180;

    private float m_numClient;
    public float _numClient
    {
        get
        {
            return m_numClient;
        }
        set
        {
            _numClientText.text = value.ToString();
            m_numClient = value;
        }
    }
    [HideInInspector]
    public bool _startGame=false;

    private bool _isDirty;

    public Transform _listItemParent;
    public static Station instance;
    private int _countItem;
    public GameObject _arrowObject;
    private void Awake()
    {
        //SetAnimRobot
        if(RedRobotAnim)
        RedRobotAnim.SetBool("CheckTalk", true);
        if(SmallRobotAnim)
        SmallRobotAnim.SetBool("CheckTalk", true);
        
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        GameObject canvas = transform.Find("Canvas").gameObject;
        _orderUI = canvas.transform.Find("OrderUI").GetComponent<OrderUI>();
        //_wrong = canvas.transform.Find("wrong").gameObject;
        //correct = canvas.transform.Find("correct").gameObject;
        _center = transform.Find("Center").gameObject;

        _craft = transform.parent.transform.Find("CraftZone").GetComponent<CraftContainer>();

        _mainDishPoint = _center.transform.Find("pointMainDish").gameObject;
        _drinkPoint = _center.transform.Find("pointWater").gameObject;
        _sideDishPoint = _center.transform.Find("pointSideDish").gameObject;

        _timerText = canvas.transform.Find("Time").GetComponent<TextMeshProUGUI>();
        _sumScoreText = canvas.transform.Find("SumScoreText").GetComponent<TextMeshProUGUI>();
        _numClientText = canvas.transform.Find("NumClient").GetComponent<TextMeshProUGUI>();

        //_CanvasResult = transform.parent.transform.Find("CanvasLeaderboard").gameObject;
        _CanvasResult.gameObject.SetActive(false);

        _typeContainer._mainDish = canvas.transform.Find("MainDish").GetComponent<TypeContainer>();
        _typeContainer._sideDish = canvas.transform.Find("SideDish").GetComponent<TypeContainer>();
        _typeContainer._drink = canvas.transform.Find("Beverage").GetComponent<TypeContainer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(StartGame());
        ReadOrder(_currentRequirement);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            FinishOrder();
        }
        
    }

    public void ReadOrder(Requirement requirement)
    {
        requirement._client.SetOrder();

        if (requirement._client._sound)
            SoundManager.Instance.PlaySound(requirement._client._sound);
        Debug.Log("Read Order "+requirement._client._order[0]);
        _craft.ReadRecipe(requirement._client._currentOrder._orderClientContainer[0]._order.ToString());
        _name.text = requirement._client.name;
        string strOrder = "";

        foreach (OrderClientContainer item in requirement._client._listOrder)
        {
            strOrder += "-" + item._order.ToString()+"\n";
            ReadOrderType(item);
        }
        _orderUI._orderText.text = strOrder;
        _currentRequirement = requirement;
        _numClient++;
    }

    public void ReadOrderType(OrderClientContainer order)
    {
        switch (order._orderType)
        {
            case Order.OrderType.MainDish:
                _typeContainer._mainDish._image.texture = order._imageOrder;
                break;
            case Order.OrderType.SideDish:
                _typeContainer._sideDish._image.texture = order._imageOrder;
                break;
            case Order.OrderType.Beverage:
                _typeContainer._drink._image.texture = order._imageOrder;
                break;
            case Order.OrderType.NumberOfTypes:
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Item")
        {
            Item item = other.gameObject.GetComponent<Item>();
            if (item == null )
                return;

            _currentItem = item;

            if (_currentItem._currentGrab != null)
            {
                _currentGrab = _currentItem._currentGrab;
                _currentGrab.GrabEnd();
            }
            item._rb.isKinematic = true;
            CheckEnter(item);
            _listItem.Add(item);

            GameObject paticle = Instantiate(_paticle, _pointer.position, _paticle.transform.rotation);
            Destroy(paticle, 5);
            /*
            Destroy(item.gameObject);
            _manager.RandomRequirment();
            _currentRequirement = null;*/
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Item")
        {
            Item item = other.gameObject.GetComponent<Item>();
            if (item == null)
                return;
            CheckExit(item);
            if (_listItem.Contains(item))
            {
                _listItem.Remove(item);
            }
        }
    }

    public void FinishOrder()
    {
        if (_isDirty == true)
            return;
        StartCoroutine(DoneTaskOrder());
    }

    public void RemoveOrder()
    {
        foreach (Item item in _listItem)
        {
            Destroy(item.gameObject);
        }
        _listItem.Clear();

        _currentItem = null;
    }
    
    public void OnButtonPress(string function)
    {
        if (function == "SendItem")
        {
            FinishOrder();
        }

        if (function == "RemoveItem")
        {
            RemoveOrder();
        }
    }

    public bool CheckItem()
    {
        if (_currentItem != null)
        {
            _tempListOrder.Clear();
            foreach (OrderClientContainer item in _currentRequirement._client._listOrder)
            {
                _tempListOrder.Add(item._order);
            }

            foreach (Item item in _listItem)
            {
                if (_tempListOrder.Contains(item._currentOrder))
                {
                    _tempListOrder.Remove(item._currentOrder);
                    _sumScore += item._score;
                    //_listItem.Remove(item);
                }

                if (_tempListOrder.Count <= 0)
                {
                    return true;
                }
            }
        }
        /*if (_currentItem._water == _currentRequirement._client._currentOrder._water &&
            _currentItem._wave == _currentRequirement._client._currentOrder._wave&&
            _currentItem._currentOrder == _currentRequirement._client._currentOrder._order)
            return true;*/
        return false;
    }

    public IEnumerator DoneTaskOrder()
    {
        RedRobotAnim.SetBool("CheckTalk", false);
        RedRobotAnim.SetTrigger("CheckJump");
        RedRobotAnim.SetBool("Happy",true);
        SmallRobotAnim.SetBool("CheckTalk", false);
        SmallRobotAnim.SetTrigger("CheckJump");
        SmallRobotAnim.SetBool("Happy",true);
        
        bool check = CheckItem();
        _scoreText.gameObject.SetActive(true);
        SoundManager.Instance._sounds[1].PlaySound();
        if (check)
        {
            _scoreText.text = _sumScore.ToString();
            //correct.gameObject.SetActive(check);
        }/*
        else
        {
            _scoreText.text = "0";
            //_wrong.gameObject.SetActive(true);
        }*/
        _isDirty = true;
        yield return new WaitForSeconds(2);

        foreach (Item item in _listItem)
        {
            _countItem--;
            Destroy(item.gameObject);
        }
        _scoreText.gameObject.SetActive(false);
        _listItem.Clear();
        _currentItem = null;
        _currentRequirement = null;
        _manager.RandomRequirment();
        //correct.gameObject.SetActive(false);
        //_wrong.gameObject.SetActive(false);
        _isDirty = false;
        
        RedRobotAnim.SetBool("Happy",false);
        SmallRobotAnim.SetBool("Happy",false);
        SmallRobotAnim.SetBool("CheckTalk", true);
        RedRobotAnim.SetBool("CheckTalk", true);

        _dishContainer.Reset();
        _typeContainer.Reset();
        CheckItemCount();
    }

    public void CheckEnter(Item item)
    {
        switch (item._currentItemType)
        {
            case Item.TypeItem.MainDish:
                item.transform.position = _mainDishPoint.transform.position;
                item.transform.rotation = _mainDishPoint.transform.rotation;
                _dishContainer._mainDish = item;
                foreach (OrderClientContainer order in _currentRequirement._client._listOrder)
                {
                    if(item._currentOrder == order._order)
                    {
                        _typeContainer._mainDish._send.gameObject.SetActive(true);
                    }
                }
                _countItem++;
                break;
            case Item.TypeItem.SideDish:
                item.transform.position = _sideDishPoint.transform.position;
                item.transform.rotation = _sideDishPoint.transform.rotation;
                _dishContainer._sideDish = item;
                foreach (OrderClientContainer order in _currentRequirement._client._listOrder)
                {
                    if (item._currentOrder == order._order)
                    {
                        _typeContainer._sideDish._send.gameObject.SetActive(true);
                    }
                }
                _countItem++;
                break;
            case Item.TypeItem.item:
                break;
            case Item.TypeItem.Drink:
                item.transform.position = _drinkPoint.transform.position;
                item.transform.rotation = _drinkPoint.transform.rotation;
                _dishContainer._drink = item;
                foreach (OrderClientContainer order in _currentRequirement._client._listOrder)
                {
                    if (item._currentOrder == order._order)
                    {
                        _typeContainer._drink._send.gameObject.SetActive(true);
                    }
                }
                _countItem++;
                break;
            case Item.TypeItem.MRE:
                break;
            case Item.TypeItem.frozenFood:
                break;
            case Item.TypeItem.Tortilla:
                break;
            case Item.TypeItem.pun:
                break;
            default:
                break;
        }
        SoundManager.Instance._sounds[7].PlaySound();
        CheckItemCount();
    }

    public void CheckExit(Item item)
    {
        switch (item._currentItemType)
        {
            case Item.TypeItem.MainDish:
                _dishContainer._mainDish = null;
                _typeContainer._mainDish._send.gameObject.SetActive(false);
                _countItem--;
                break;
            case Item.TypeItem.SideDish:
                _dishContainer._sideDish = null;
                _typeContainer._sideDish._send.gameObject.SetActive(false);
                _countItem--;
                break;
            case Item.TypeItem.item:
                break;
            case Item.TypeItem.Drink:
                _dishContainer._drink = null;
                _typeContainer._drink._send.gameObject.SetActive(false);
                _countItem--;
                break;
            case Item.TypeItem.MRE:
                break;
            case Item.TypeItem.frozenFood:
                break;
            case Item.TypeItem.Tortilla:
                break;
            case Item.TypeItem.pun:
                break;
            default:
                break;
        }
        CheckItemCount();
    }

    public void CheckItemCount()
    {
        if (_countItem >= 3)
        {
            _arrowObject.gameObject.SetActive(true);
        }else if (_countItem < 3)
        {
            _arrowObject.gameObject.SetActive(false);
        }
    }

    public IEnumerator StartGame()
    {
        _startGame = true;
        float minutes = 0;
        float seconds = 0;

        string minStr = "";
        string secondStr = "";
        while (_startGame)
        {
            _time -= Time.deltaTime;
            
            minutes = Mathf.Floor(_time / 60);
            seconds = Mathf.RoundToInt(_time % 60);
            if (minutes < 10)
            {
                minStr = "0" + minutes.ToString();
            }
            if (seconds < 10)
            {
                secondStr = "0"+Mathf.RoundToInt(seconds).ToString();
                //secondStr = "0" + Mathf.Round(seconds).ToString();
            }else if (seconds >= 10)
            {
                secondStr = seconds.ToString();
            }
            _timerText.text = minStr + " : " + secondStr;
            if (_time <= 0)
            {
                StartCoroutine(EndGame());
                _timerText.text = "Time Out";
            }
            yield return null;
        }
        
    }

    public IEnumerator EndGame()
    {
        _startGame = false;
        _CanvasResult.SetActive(true);
        SoundManager.Instance._sounds[8].PlaySound();
        yield return null;
    }
}
[System.Serializable]
public class DishContainer
{
    public Item _mainDish;
    public Item _sideDish;
    public Item _drink;

    public void Reset()
    {
        _mainDish = null;
        _sideDish = null;
        _drink = null;
    }
}
[System.Serializable]
public class TypeContainerData
{
    public TypeContainer _mainDish, _drink, _sideDish;
    public void Reset()
    {
        _mainDish._send.gameObject.SetActive(false);
        _sideDish._send.gameObject.SetActive(false);
        _drink._send.gameObject.SetActive(false);
    }
}



