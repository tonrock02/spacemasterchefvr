using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Client:MonoBehaviour {
    public string name;
    public List<Order> _order = new List<Order>();
    public List<OrderClientContainer> _listOrder;
    public Order _currentOrder;
    public AudioClip _sound;
    public void RadomOrder()
    {
        int random = (int)Random.Range(0, _order.Count);
        Debug.Log("Random Menu " + random);
        SetOrder();
        _currentOrder = _order[random];
        
        //_currentTypeOrder = (Order.OrderClient)Random.Range(0, (int)Order.OrderClient.NumberOfTypes);
    }

    public void SetOrder()
    {
        _currentOrder = _order[0];
        _listOrder = _currentOrder._orderClientContainer;
    }
}

