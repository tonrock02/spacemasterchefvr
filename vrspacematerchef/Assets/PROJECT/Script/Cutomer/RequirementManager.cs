using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequirementManager : MonoBehaviour
{
    public List<Requirement> _requirments = new List<Requirement>();
    public Requirement _currentRequirment;

    public Station _station;
    // Start is called before the first frame update
    void Start()
    {
        //RandomRequirment();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RandomRequirment()
    {
        int indexRandom = Random.Range(0, _requirments.Count);

        _currentRequirment = _requirments[indexRandom];

        _currentRequirment._client.RadomOrder();
        //_currentRequirment._client._currentOrder = _currentRequirment._client._order[indexRandom];
        _station.ReadOrder(_currentRequirment);
    }
}


[System.Serializable]
public class Requirement
{
    public Client _client;
}


