﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Order {
    public enum OrderClient
    {
        สลัดไก่,
        สลัดกุ้ง,
        สลัดเป็ด,
        ตอติล่าไส้ไก่,
        ตอติล่าไส้เนื้อ,
        ตอติล่าไส้กุ้ง,
        ลาซานย่า,
        สปาเก็ตตี้,
        พายแอปเปิ้ล,
        บราวนี่,
        ผักโขม,
        กะหล่ำ,
        แครอท,
        มะเขือเทศ,
        พิซซ่าหน้าเนื้อ,
        พิซซ่าหน้ากุ้ง,
        พิซซ่าหน้าไก่,
        กาแฟ,
        โกโก้,
        น้ำส้ม,
        นม,
        Brownie,
        Orange,
        Spagetti,
        Salad,
        Tortilla,
        Pizza,
        Flour,
        Raw,
        Meat,
        Vegetable,
        Water,
        NumberOfTypes
    }

    public enum OrderType
    {
        MainDish,
        SideDish,
        Beverage,
        Meat,
        NumberOfTypes
    }
    public List<OrderClientContainer> _orderClientContainer;
    [HideInInspector]
    public bool _wave;
    [HideInInspector]
    public bool _water;
}
[System.Serializable]
public class OrderClientContainer
{
    public Texture _imageOrder;
    
    public Order.OrderClient _order;

    
    public Order.OrderType _orderType;
}
