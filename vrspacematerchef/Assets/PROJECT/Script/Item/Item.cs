using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public abstract class Item : MonoBehaviour
{
    public string name;

    public BoxCollider _col;
    public Rigidbody _rb;
    public Outline _line;

    public bool hover,done;

    //[HideInInspector]
    public GrabController _currentGrab;
    public enum TypeItem
    {
        Meat,MainDish,SideDish,item,Drink,MRE,frozenFood,Tortilla,pun
    }

    public TypeItem _currentItemType;

    public List<TypeTask> _listTask = new List<TypeTask>();

    public enum TypeTask
    {
        AddWater,Squeeze,Microwave,Tear,MixFood,Done
    }

    public TypeTask _currentTask;
    public int indexTask = 0;
    protected float m_prevFlex;

    
    public OVRInput.Controller m_controller;

    float grabBegin = 0.55f;
    float grabEnd = 0.35f;

    //[HideInInspector]
    public Slider _squeezeBar;

    public float m_squeeze;
    public float _squeeze
    {
        get
        {
            return m_squeeze;
        }
        set
        {
            _squeezeBar.value = value;
            m_squeeze = value;
        }
    }

    public Order.OrderClient _completeOrder;
    public Order.OrderClient _currentOrder;

    public float _maxSqueeze;

    [HideInInspector]
    public bool _wave;
    [HideInInspector]
    public bool _water;

    public float _score;

    public Animator _animation;

    public Vector3 _startScale;
    public GameObject _model;
    private List<Material> _mats = new List<Material>();

    public WaterCooked _texture;

    public enum SetTexture
    {
        water,cooked
    }

    private void Awake()
    {
        _col = transform.GetComponent<BoxCollider>();
        _rb = transform.GetComponent<Rigidbody>();
        //_line = transform.GetComponent<Outline>();
        _currentTask = _listTask[0];

        //_currentOrder = Order.OrderClient.Raw;
        _startScale = transform.localScale;
        Renderer rend = _model.GetComponent<Renderer>();

        foreach (Material item in rend.materials)
        {
            _mats.Add(item);
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    protected void CheckForGrabOrRelease(float prevFlex,System.Action<int> callback)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            callback.Invoke(0);
            //Start
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            //End
        }
    }

    

    public virtual void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Grab")
        {
            _currentGrab = other.GetComponent<GrabController>();

            if (_currentGrab != null)
                m_controller = _currentGrab.m_controller;
            if (_line != null)
                _line.enabled = true;
            hover = true;
            
        }

        if (other.gameObject.tag == "item")
        {
            Item_Food food = other.GetComponent<Item_Food>();
            if (food != null)
                if (_currentItemType == TypeItem.pun && food._currentItemType == TypeItem.Tortilla) 
                {
                    OnInteracItem(food);
                }
        }
        
    }

    public virtual void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            hover = true;
        }
    }

    public virtual void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            hover = false;
            if (_line != null)
                _line.enabled = false;
            _currentGrab = null;
        }
    }

    protected abstract void OnInteracItem(Item item);

    public void DoneTask()
    {
        indexTask++;

        if (indexTask < _listTask.Count)
        {
            _currentTask = _listTask[indexTask];
        }

        if(indexTask >= _listTask.Count)
        {
            done = true;
            _currentTask = TypeTask.Done;
            //_currentOrder = _completeOrder;
        }
        
    }
    
    public void SetTextureFunction(SetTexture type)
    {
        switch (type)
        {
            case SetTexture.water:
                foreach (Material item in _mats)
                {
                    if(_texture._water)
                        item.mainTexture = _texture._water;
                }

                break;
            case SetTexture.cooked:
                foreach (Material item in _mats)
                {
                    if (_texture._cooked)
                        item.mainTexture = _texture._cooked;
                }
                break;
            default:
                break;
        }
    }

}

[System.Serializable]
public class WaterCooked
{
    public Texture _water;
    public Texture _cooked;
}
