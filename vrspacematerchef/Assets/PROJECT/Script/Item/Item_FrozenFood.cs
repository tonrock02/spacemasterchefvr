using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item_FrozenFood : Item
{
    protected override void OnInteracItem(Item item)
    {
        
    }
    private void Awake()
    {

        //_squeezeBar = transform.Find("Canvas").transform.Find("Slider").GetComponent<Slider>();
        //_squeezeBar.maxValue = _maxSqueeze;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_currentTask == TypeTask.Squeeze)
        {
            if (_currentGrab == null)
                return;

            //_squeezeBar.gameObject.SetActive(true);
            float prevFlex = m_prevFlex;
            // Update values from inputs
            m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, m_controller);
            CheckForGrabOrRelease(prevFlex, delegate {
                //Squeezing();
            });
        }
    }

    public void Squeezing()
    {
        m_squeeze += 1;
        Debug.Log("Squeez!!");
        if (m_squeeze >= _maxSqueeze)
        {
            DoneTask();
            _squeezeBar.gameObject.SetActive(false);
        }
    }
}
