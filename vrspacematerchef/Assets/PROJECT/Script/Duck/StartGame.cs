using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayMaker;

public class StartGame : MonoBehaviour
{
    public PlayMakerFSM MyFSM;
    //private bool startGamePlay;

    void Start()
    {
       //startGamePlay = false; 
    }

    void Update()
    {
        // if (startGamePlay == true)
        // {
        //     StartCoroutine(Station.instance.StartGame());
        // }
    }

    public void OnButtonPress(string function)
    {
        if (function == "StartGame")
        {
            Debug.Log("Start Game");
            Begin();

        }
    }

    public void Begin()
    {
        
        Debug.Log("Start Gameplay");
        MyFSM.SendEvent("StartGame");

        StartCoroutine(Station.instance.StartGame());
        //startGamePlay = true;
    }
}