using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NextPicture : MonoBehaviour
{
    //set up text fields
    public TextMeshProUGUI displayInfo;
    public RawImage theImage;

    //set up arrays
    public string[] info = { "info1", "info2", "info3", "info4", "info5", "info6", "info7", "info8"};
    public Texture[] myTexture = new Texture[8];

    private int currentItem = 0;

    void Start()
    {
        displayInfo.text = info[currentItem];
        theImage.texture = myTexture[currentItem];
    }

    public void OnButtonPress(string function)
    {
        if (function == "NextPic")
        {
            NextButton();
        }
    }

    public void NextButton()
    {
        Debug.Log("Next");
        currentItem++;

        //check if at end of array
        if(currentItem > info.Length -1)
        {
            currentItem = 0;
        }

        displayInfo.text = info[currentItem];
        theImage.texture = myTexture[currentItem];
    }
}
