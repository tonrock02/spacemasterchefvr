using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayMaker;

public class LinkButton2 : MonoBehaviour
{
    public PlayMakerFSM MyFSM;

    void Update()
    {

    }

    public void OnButtonPress(string function)
    {
        if (function == "NextText")
        {
            //Debug.Log("Press Next Text");
            NextText();

        }
    }

    public void NextText()
    {
        //Debug.Log("Send Event");
        MyFSM.SendEvent("FINISHED");
       // MyFSM.SendEvent("StartGame");
    }
}