using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonVR2 : MonoBehaviour
{
    public Outline _line;
    public bool hover = false;

    public GrabController _grab;

    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    protected float m_prevFlex;

    public OVRInput.Controller m_controller;
    public string _function;

    public NextPicture _nextPic;
    public LinkButton2 _nextText;
    public StartGame _startGame;
    private void Update()
    {
        
            float prevFlex = m_prevFlex;
            // Update values from inputs
            m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, m_controller);

            CheckForGrabOrRelease(prevFlex);
        
    }

    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
        }
    }

    public void GrabBegin()
    {
        Debug.Log("Press");

        if (_nextPic)
        _nextPic.OnButtonPress(_function);

        if (_nextText)
        _nextText.OnButtonPress(_function);

        if (_startGame)
        _startGame.OnButtonPress(_function);
    }

    public void GrabEnd()
    {

    }
}