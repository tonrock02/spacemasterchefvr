using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenInfo : MonoBehaviour
{
    public GameObject infoGraphic;

    void Start()
    {
        infoGraphic.SetActive(false);
    }

    public void OnButtonPress(string function)
    {
        if (function == "OpenInfo")
        {
            DataInfo();
        }

    }

    public void DataInfo()
    {
        infoGraphic.SetActive(true);
    }
}
