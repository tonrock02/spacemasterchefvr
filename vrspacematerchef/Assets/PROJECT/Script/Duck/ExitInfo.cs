using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitInfo : MonoBehaviour
{
    public GameObject exitInfo;

    void Start()
    {

    }

    public void OnButtonPress(string function)
    {
        if (function == "ExitInfo")
        {
            ExitDataInfo();
        }
    }

    public void ExitDataInfo()
    {
        exitInfo.SetActive(false);
    }
}
