using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayMaker;

public class LinkButton : MonoBehaviour
{
    private bool isClickButton;
    public PlayMakerFSM MyFSM;

    void Start()
    {
        isClickButton = false;
    }

    void Update()
    {
        isClickButton = OVRInput.Get(OVRInput.Button.Two);

        if (isClickButton == true)
        {
            isClickButton = true;

            ClickButton();
        }
    }

    public void ClickButton()
    {
        MyFSM.SendEvent("ButtonClick");
        MyFSM.SendEvent("ButtonClick 2");
    }
}
