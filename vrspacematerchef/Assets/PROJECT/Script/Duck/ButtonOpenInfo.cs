using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonOpenInfo : MonoBehaviour
{
    //public Outline _line;
    public bool hover = false;

    public GrabController _grab;

    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    protected float m_prevFlex;

    public OVRInput.Controller m_controller;

    public string _function;

 
    public OpenInfo _openInfo;
    
    //public ExitInfo _exitInfo;

    public GameObject uiObject;

    void Start()
    {
        uiObject.SetActive(false);
    }

    private void Update()
    {
        if (hover)
        {
            float prevFlex = m_prevFlex;
            // Update values from inputs
            m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

            CheckForGrabOrRelease(prevFlex);
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            if (_openInfo)
                _openInfo.OnButtonPress(_function);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _grab = other.GetComponent<GrabController>();
            if (_grab == null)
                return;

            /*if (_station._currentItem == null)
                return;*/

            m_controller = _grab.m_controller;
            //_line.enabled = true;
            hover = true;

            uiObject.SetActive(true);
            Debug.Log("Trigger");

        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
           // _line.enabled = false;
            hover = false;
            _grab = null;

            uiObject.SetActive(false);
            Debug.Log("Exit");
        }
    }

    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
        }
    }

    public void GrabBegin()
    {

    }

    public void GrabEnd()
    {

        if (_openInfo)
        _openInfo.OnButtonPress(_function);

        // if (_exitInfo)
        // _exitInfo.OnButtonPress(_function);

    }
}
