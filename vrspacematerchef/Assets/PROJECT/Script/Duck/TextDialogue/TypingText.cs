using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("MyAction")]
public class TypingText : ComponentAction<UnityEngine.UI.Text>
{
    public FsmOwnerDefault gameObject;
    public Text _text;
    public FsmBool resetOnExit;
    public bool everyFrame;
    public float typingSpeed;
    private UnityEngine.UI.Text uiText;
	private string originalString;
    public override void Reset()
		{
			gameObject = null;
			resetOnExit = null;
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			
		}
		
		public override void OnUpdate()
		{
			//DoSetTextValue();
		}

	    // private void DoSetTextValue()
		// {
        //     yield return new WaitForSeconds(typingSpeed);
		// }

		public override void OnExit()
		{
			if (uiText == null) return;
			
			if (resetOnExit.Value)
			{
				uiText.text = originalString;
			}
		}

// #if UNITY_EDITOR
//         public override string AutoName()
//         {
//             return ActionHelpers.AutoName("UISetText", text);
//         }
// #endif
}
}