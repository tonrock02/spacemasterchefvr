using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartDelay : MonoBehaviour
{
    void Start()
    {
        StartCoroutine ("Delay");
    }
    
    void Update()
    {
        
    }
    IEnumerator Delay()
    {
        Time.timeScale = 0;
        float pauseTime = Time.realtimeSinceStartup + 1.0f;
        while (Time.realtimeSinceStartup < pauseTime)
            yield return 0;
        Time.timeScale = 1;
    }
}


//public GameObject textBtnIsClick;
    //public GameObject textDelayAction;

    //private void OnMouseDown()
    //{
    //    textBtnIsClick.SetActive(true);
    //    Invoke("Action", 3.0f);
    //}

    //private void Action()
    //{
    //    textDelayAction.SetActive(true);
    //}