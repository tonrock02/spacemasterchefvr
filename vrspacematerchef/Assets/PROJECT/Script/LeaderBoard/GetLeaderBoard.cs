using System;
using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker.Actions;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.UI;

public class GetLeaderBoard : MonoBehaviour
{
    public GameObject scorePrefab;
    public GameObject scoreHolder;
    private string url = "https://us-central1-test-87c31.cloudfunctions.net/getleaderboard";
    // Start is called before the first frame update
    public GetData Data = new GetData();


    void Start()
    {
        
    }

    void OnEnable()
    {
        StartCoroutine(GetDataProcess());
    }

    void OnDisable()
    {
        DeleteScoreList();
    }

    IEnumerator GetDataProcess()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            //Debug.Log("All Json String : " + www.downloadHandler.text);
            Data = JsonUtility.FromJson<GetData>(www.downloadHandler.text);
            for (int i = 0; i < 10; i++)
            {
                scorePrefab.name = "Item " + i;
                scorePrefab.transform.Find("Score").GetComponent<TextMeshProUGUI>().SetText(Data.leaderboard[i].score.ToString());
                scorePrefab.transform.Find("Name").GetComponent<TextMeshProUGUI>().SetText((i+1) + ". " + Data.leaderboard[i].name);
                Instantiate (scorePrefab, transform);
                //Debug.Log("Name : " + Data.leaderboard[i].name);
                //Debug.Log("Score : " + Data.leaderboard[i].score);
            }
        }
    }

    [System.Serializable]
    public class GetData
    {
        public GetSubData[] leaderboard;
    }

    [System.Serializable]
    public class GetSubData
    {
        public string name;
        public int score;
    }

    public void DeleteScoreList()
    {
        var scorelist = new List<GameObject>();
        foreach (Transform child in scoreHolder.transform) scorelist.Add(child.gameObject);
        {
            scorelist.ForEach(child => Destroy(child));
        }
    }
}
