using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public AudioSource _audioSource;
    public List<SoundCotainer> _sounds = new List<SoundCotainer>();
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        foreach (SoundCotainer item in _sounds)
        {
            item.audio = _audioSource;
        }

    }

    public void PlaySound(AudioClip _clip)
    {
        if(_audioSource)
        _audioSource.PlayOneShot(_clip);
    }
}
[System.Serializable]
public class SoundCotainer
{
    public string _name;
    public AudioClip _clip;
    [HideInInspector]
    public AudioSource audio;
    public void PlaySound()
    {
        audio.PlayOneShot(_clip);
    }

    public void StopSound()
    {
        audio.Pause();
    }
}
