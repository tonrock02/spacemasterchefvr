﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.Networking;
public class CraftContainer : MonoBehaviour
{
    public List<AmountItem> _container = new List<AmountItem>();
    public List<Item> _results = new List<Item>();

    public List<Recipe> _recipes = new List<Recipe>();

    private Outline _line;
    private Transform _craftPoint;
    private bool hover;
    [HideInInspector]
    public List<Item> _itemInZone = new List<Item>();
    public GameObject _wastFood;

    public GameObject _paticle, _scorePrefab;
    public Recipe _currentRecipe;
    private Station _station;

    private TextMeshProUGUI _textRecipe;
    private int _countItem;
    public GameObject _arrowObject;
    private void Awake()
    {
        _line = transform.Find("Zone").gameObject.GetComponent<Outline>();
        _craftPoint = transform.Find("CraftPoint");
        _station = transform.parent.transform.Find("Station").GetComponent<Station>();
        _textRecipe = transform.Find("Canvas").transform.Find("textRecipe").GetComponent<TextMeshProUGUI>();

    }
    public void AddItem(Item itemIn)
    {
        _itemInZone.Add(itemIn);
        foreach (AmountItem item in _container)
        {
            if (item._itme.name == itemIn.name)
            {
                item._amount++;
            }
        }
        _countItem++;
        CheckItemInRecipe();
        CheckItemCount();
    }

    public void RemoveItem(Item itemOut)
    {
        if (_itemInZone.Contains(itemOut))
            _itemInZone.Remove(itemOut);

        foreach (AmountItem item in _container)
        {
            if (item._itme.name == itemOut.name)
            {
                item._amount--;
                if (item._amount <= 0)
                    item._amount = 0;
            }
        }
        CheckItemInRecipe();
        _countItem--;
        CheckItemCount();
    }

    public void Craft()
    {
        foreach (Recipe recipe in _recipes)
        {
            recipe.Craft(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _line.enabled = true;
            hover = true;
        }

        if (other.gameObject.tag == "Item")
        {
            Item item = other.gameObject.GetComponent<Item>();
            if (item == null)
                return;
            item._rb.isKinematic = true;
            AddItem(item);
            SoundManager.Instance._sounds[7].PlaySound();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _line.enabled = false;
            hover = false;
        }

        Item item = other.gameObject.GetComponent<Item>();
        if (item == null)
            return;

        RemoveItem(item);
    }

    public void ResultCraft(GameObject ResultGameOgject)
    {
        float _sumScore=0;
        foreach (Item item in _itemInZone)
        {
            _sumScore += item._score;
            Tweener t = item.transform.DOMove(_craftPoint.transform.position, 0.75f);
            GameObject _scoreText = Instantiate(_scorePrefab, item.transform.position, Quaternion.identity);
            _scoreText.transform.Find("text").gameObject.GetComponent<TextMeshProUGUI>().text = "+" + item._score;
            _scoreText.transform.DOMoveY(Random.Range(1,1.5f), 3);
            Destroy(_scoreText, 3);

            t.OnComplete(delegate {
                RemoveItem(item);
                t.Kill();
                _countItem--;
                Destroy(item.gameObject);
            });
        }
        foreach (AmountItem item in _container)
        {
            item._amount = 0;
        }
        //result craft item.
        GameObject result = Instantiate(ResultGameOgject, _craftPoint.position, ResultGameOgject.transform.rotation);
        result.transform.localScale = new Vector3(0, 0, 0);
        result.transform.DOScale(new Vector3(1, 1, 1), 0.75f);
        result.gameObject.GetComponent<Item>()._score += _sumScore;
        GameObject paticle = Instantiate(_paticle, _craftPoint.transform.position, Quaternion.identity);
        Destroy(paticle, 5);
        SoundManager.Instance._sounds[3].PlaySound();

        CheckItemCount();
    }

    public void OnButtonPress(string function)
    {
        if (function == "Craft")
        {
            Craft();
        }
    }

    public void ReadRecipe(string recipeName)
    {
        Debug.Log("Read Recipe " + recipeName);
        foreach (Recipe item in _recipes)
        {
            if (item.name == recipeName)
            {
                _currentRecipe = item;
            }
        }

        if (_currentRecipe != null)
        {
            string str = "";
            str += _currentRecipe.name+"\n";
            foreach (AmountItem item in _currentRecipe._material)
            {
                str += "-"+item._itme.name + " "+item._amount+"\n";
            }
            _textRecipe.text = str;
        }
        else
        {
            _textRecipe.text = "";
        }
    }

    public string CheckStatusItem(Item item)
    {
        string str = "";

        if(item._currentTask == Item.TypeTask.Microwave)
        {
            return "สุก";
        }
        if (item._currentTask == Item.TypeTask.AddWater)
        {
            return "ใส่น้ำ";
        }

        return str;
    }

    public void CheckItemInRecipe()
    {
        if (_currentRecipe != null)
        {
            string str = "";
            str += _currentRecipe.name + "\n";
            foreach (AmountItem item in _currentRecipe._material)
            {
                var addStr = "";
                foreach (AmountItem container in _container)
                {
                    if(container._itme.name == item._itme.name)
                    {
                        if(container._amount == item._amount)
                        {
                            addStr = "<s>" + "-" + item._itme.name +" "+ item._amount + "</s>" + "\n";
                        }
                        else
                        {
                            addStr = "-" + item._itme.name +" " + item._amount + "\n";
                        }
                    }
                }
                str += addStr;
            }
            _textRecipe.text = str;
        }
    }

    public void CheckItemCount()
    {
        if (_countItem >= _currentRecipe._material.Count)
        {
            _arrowObject.gameObject.SetActive(true);
        }
        else if (_countItem < _currentRecipe._material.Count)
        {
            _arrowObject.gameObject.SetActive(false);
        }
    }
}
