using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class AmountItem
{
    public Item _itme;
    public int _amount;
}

[CreateAssetMenu]
public class Recipe : ScriptableObject
{
    public string name;
    public List<AmountItem> _material = new List<AmountItem>();
    public List<AmountItem> _result = new List<AmountItem>();
    public GameObject _resultGameObject;
    public bool CanCraft(CraftContainer container)
    {
        foreach (AmountItem item in _material)
        {
            foreach (AmountItem containerItem in container._container)
            {
                if (item._itme.name == containerItem._itme.name)
                {
                    if (containerItem._amount < item._amount)
                        return false;
                }
            }
        }
        return true;
    }

    public void Craft(CraftContainer container)
    {
        if (CanCraft(container))
        {
            container.ResultCraft(_resultGameObject);
        }
    }
}
