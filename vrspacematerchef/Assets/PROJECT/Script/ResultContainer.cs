using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class ResultContainer : MonoBehaviour
{
    public static ResultContainer _instance;

    public TextMeshProUGUI _scoreText;
    public TextMeshProUGUI _nameText;
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(this.gameObject);

        _scoreText = transform.Find("LeaderBoard").transform.Find("MyScore").transform.Find("My").GetComponent<TextMeshProUGUI>();
    }
    // Start is called before the first frame update
    void Start()
    {
        

    }

    public void ResultScore(float score,string name)
    {
        _scoreText.text = score.ToString();
        _nameText.text = name;
    }

    public void OnButtonPress(string function)
    {
        if (function == "Restart")
        {
            StartCoroutine(Restart());
        }

    }

    IEnumerator Restart()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Gameplay_new");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
