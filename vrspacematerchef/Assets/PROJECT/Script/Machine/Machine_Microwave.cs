﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Machine_Microwave : MachineMain
{
    bool wave = false, done = false;
    public Item _currentItem;
    public float _maxTimeWave;
    public float m_time;
    public float _time
    {
        get
        {
            return m_time;
        }
        set
        {
            _slider.value = value;
            m_time = value;
        }
    }
    public Outline _line;
    public bool hover = false;
    public GrabController _currentGrab;
    public Slider _slider;
    public GameObject _paticle;
    // Start is called before the first frame update
    void Start()
    {
        _slider = transform.Find("Canvas").transform.Find("Slider").GetComponent<Slider>();
        _slider.maxValue = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (hover && done)
        {
            /*if (_currentGrab.grab)
            {
                done = false;

                _currentItem.gameObject.SetActive(true);
                _currentItem.transform.position = _currentGrab.transform.position;
                _currentGrab.grab = false;
                //_currentGrab._grabObject.Add(_currentItem.GetComponent<GrabObject>());
                _currentGrab.GrabBegin();

                _currentItem = null;
                _currentGrab = null;

                hover = false;
                _line.enabled = false;

                _time = 0;
                _slider.gameObject.SetActive(false);
            }*/
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!done && _currentItem == null)
        {
            if (other.gameObject.tag == "Item")
            {
                Item item = other.gameObject.GetComponent<Item>();
                if (item == null || item._currentTask != _currentMachineTask)
                    return;

                OnItemEnter(item);
            }
        }

        if (other.gameObject.tag == "Grab")
        {
            _line.enabled = true;
            hover = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _line.enabled = false;
            hover = false;
        }
    }

    public IEnumerator Waving()
    {
        wave = true;
        _time = 0;
        _line.enabled = false;
        bool increasing = false;
        while (wave)
        {

            if (increasing)
            {
                _time += Time.deltaTime;
                if (_time > _slider.maxValue)
                    increasing = false;
            }
            else
            {
                _time -= Time.deltaTime;
                if (_time <= 0)
                    increasing = true;
            }

            if (hover)
            {
                if (OVRInput.Get(OVRInput.Button.Three) || OVRInput.Get(OVRInput.Button.One))
                {
                    wave = false;
                    done = true;
                    _currentItem.SetTextureFunction(Item.SetTexture.cooked);
                    SetUpScore(_currentItem, _slider.value);
                    _currentItem.name += "สุก";
                    _currentItem.gameObject.tag = "Item";
                    _currentItem._wave = true;
                    _currentItem.DoneTask();
                    SoundManager.Instance._sounds[0].PlaySound();
                    GameObject paticle = Instantiate(_paticle, transform.position + new Vector3(0, 0, -1), _paticle.transform.rotation);
                    Destroy(paticle, 5);
                    //////////////////
                    ///
                    done = false;

                    _currentItem.gameObject.SetActive(true);
                    _currentItem.transform.position = _currentGrab.transform.position;
                    _currentGrab.grab = false;
                    //_currentGrab._grabObject.Add(_currentItem.GetComponent<GrabObject>());
                    _currentGrab.GrabBegin();

                    _currentItem = null;
                    //_currentGrab = null;

                    hover = false;
                    _line.enabled = false;

                    _time = 0;
                    _slider.gameObject.SetActive(false);
                }
            }

            yield return null;
        }
    }

    public void OnItemEnter(Item item)
    {
        if (item.hover)
        {
            _currentItem = item;
            _currentItem.gameObject.SetActive(false);
            StartCoroutine(Waving());
            _slider.gameObject.SetActive(true);

            if (_currentItem._currentGrab != null)
            {
                _currentGrab = _currentItem._currentGrab;
                _currentGrab.GrabEnd();
                _currentGrab.grab = false;
            }
        }
    }

    public void SetUpScore(Item item, float value)
    {
        Debug.Log("Value " + value);
        if (value > 0.3f && value < 0.7f)
        {
            item._score *= 2;
        }

        if (value < 0.3f || value > 0.7f)
        {
            item._score *= 1;
        }
        Debug.Log("Add score To " + item.name+" Score "+item._score);
    }
}
