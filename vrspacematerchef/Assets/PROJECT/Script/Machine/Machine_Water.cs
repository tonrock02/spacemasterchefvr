﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Machine_Water : MachineMain
{
    bool wave = false, done = false;
    public Item _currentItem;
    public float _maxTimeWave;
    public float m_time;
    public float _time
    {
        get
        {
            return m_time;
        }
        set
        {
            _slider.value = value;
            m_time = value;
        }
    }
    public Outline _line;
    public bool hover = false;
    public GrabController _currentGrab;
    public Slider _slider;
    public Transform _waterPoint;

    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    protected float m_prevFlex;

    public GameObject _paticle;
    private bool press;
    private Animator _aniItem;
    // Start is called before the first frame update
    void Start()
    {
        _slider = transform.Find("Canvas").transform.Find("Slider").GetComponent<Slider>();
        _slider.maxValue = _maxTimeWave;
        
    }

    // Update is called once per frame
    void Update()
    {
        /*if (hover && done)
        {
            if (_currentGrab.grab)
            {
                
                done = false;

                _currentItem.gameObject.SetActive(true);
                _currentItem.transform.position = _currentGrab.transform.position;
                _currentItem = null;

                //_currentGrab._grabObject.Add(_currentItem.GetComponent<GrabObject>());
                _currentGrab.GrabBegin();

                
                _currentGrab = null;

                hover = false;
                _line.enabled = false;

                _time = 0;
                _slider.gameObject.SetActive(false);
                
            }
        }*/
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _line.enabled = true;
            hover = true;
        }

        if (!done && _currentItem == null)
        {
            if (other.gameObject.tag == "Item")
            {
                Item item = other.gameObject.GetComponent<Item>();
                if (item == null|| item._currentTask != _currentMachineTask)
                    return;

                OnItemEnter(item);

                
            }
        }

        

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Grab")
        {
            _line.enabled = false;
            hover = false;
        }
    }

    public void OnItemEnter(Item item)
    {
        if (item.hover)
        {
            _currentItem = item;
            //_currentItem.gameObject.SetActive(false);
            _currentItem.gameObject.tag = "Untagged";
            StartCoroutine(Watering());
            _slider.gameObject.SetActive(true);

            if (_currentItem._currentGrab != null)
            {
                _currentGrab = _currentItem._currentGrab;
                _currentGrab.GrabEnd();
                _currentGrab.grab = false;
            }
        }
    }

    public IEnumerator Watering()
    {
        wave = true;
        _time = 0;
        _line.enabled = false;
        while (wave)
        {
            _currentItem.transform.rotation = _waterPoint.transform.rotation;
            _currentItem.transform.position = _waterPoint.transform.position;
            //_time += Time.deltaTime;
            //m_prevFlex = OVRInput.Get(OVRInput.Axis1D, _currentGrab.m_controller);

            if (hover)
            {
                if (OVRInput.Get(OVRInput.Button.Three) || OVRInput.Get(OVRInput.Button.One))
                {
                    _time += Time.deltaTime;
                    if (!press)
                    {
                        SoundManager.Instance._sounds[4].PlaySound();
                        press = true;
                        if (_currentItem._animation)
                        {
                            _aniItem = _currentItem._animation;
                            _aniItem.enabled = true;
                        }
                    }

                }
                else
                {
                    if (_aniItem)
                        _aniItem.enabled = false;
                    press = false;
                }
            }

            if (_time > _maxTimeWave)
            {
                _currentItem.SetTextureFunction(Item.SetTexture.water);
                wave = false;
                done = true;
                SoundManager.Instance._sounds[0].PlaySound();
                GameObject paticle = Instantiate(_paticle, transform.position+new Vector3(0,0,-1), _paticle.transform.rotation);
                Destroy(paticle, 5);

                if (_currentItem._currentItemType != Item.TypeItem.Meat)
                    _currentItem.name += "ใส่น้ำ";

                _currentItem.gameObject.tag = "Item";
                _currentItem._water = true;
                _currentItem.DoneTask();
                press = false;

                /*if (_currentGrab)
                    _currentGrab.GrabEnd();*/

                done = false;

                _currentItem.gameObject.SetActive(true);
                _currentItem.transform.position = _currentGrab.transform.position;
                
                
                _currentGrab.grab = false;
                _currentGrab._grabObject.Add(_currentItem.gameObject.GetComponent<GrabObject>());
                //_currentGrab._grabObject.Add(_currentItem.GetComponent<GrabObject>());
                _currentGrab.GrabBegin();


                //_currentGrab = null;
                _currentItem = null;

                hover = false;
                _line.enabled = false;

                _time = 0;
                _slider.gameObject.SetActive(false);
            }
            yield return null;
        }
    }

    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
        }
    }

    public void GrabBegin()
    {
        
    }

    public void GrabEnd()
    {
        
    }
}
