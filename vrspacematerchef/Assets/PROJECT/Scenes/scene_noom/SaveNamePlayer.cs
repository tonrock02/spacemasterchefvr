using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SaveNamePlayer : MonoBehaviour
{
    private TextMeshPro Output;
    private KeyDetector _text;
    // Start is called before the first frame update
    void Start()
    {
        //Output = GameObject.FindGameObjectWithTag("SaveName").GetComponentInChildren<TextMeshPro>();
        _text = gameObject.GetComponent<KeyDetector>();
       // _text =GameObject.Get
    }

    // Update is called once per frame
    void Update()
    {
        SetName();
    }

    public void SetName()
    {
        Output.text += _text.saveName;
    }
}
