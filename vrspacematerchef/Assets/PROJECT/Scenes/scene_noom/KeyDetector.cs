using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyDetector : MonoBehaviour
{
    //private TextMeshPro playIextOutput;
    public TextMeshPro Output;
    public TextMeshProUGUI playIextOutput;
    // private TextMeshPro SaveName;
    public string saveName;
    //private TextMeshPro SaveName;
    // Start is called before the first frame update
    void Start()
    {
        //playIextOutput = GameObject.FindGameObjectWithTag("PlayerTextOutput").GetComponentInChildren<TextMeshPro>();
       // SaveName = GameObject.FindGameObjectWithTag("SaveName").GetComponentInChildren<TextMeshPro>();
    }

    private void Update()
    {
        //Output.text = "" + saveName;
    }

    // Update is called once per frame
    public void OnTriggerEnter(Collider other)
    {
        var key = other.GetComponentInChildren<TextMeshPro>();

        if (key != null)
        {
            var keyFeedback = other.gameObject.GetComponent<KeyFeedBack>();
            keyFeedback.keyhit = true;

            if (other.gameObject.GetComponent<KeyFeedBack>().keyCanBeHitTargain)
            {
                if (key.text == "SPACE")
                {
                    playIextOutput.text += " ";
                }
                else if (key.text == "<---")
                {
                    playIextOutput.text = playIextOutput.text.Substring(0, playIextOutput.text.Length - 1);
                }
                else if (key.text == "ENTER")
                {
                    saveName = playIextOutput.text;
                    KeyboardContainer.instance.Enter();
                }
                else
                {
                    playIextOutput.text += key.text;
                }
            }
        }
    }
}
