using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyFeedBack : MonoBehaviour
{
    private SoundOnClick soundOnClick;
    public bool keyhit = false;
    public bool keyCanBeHitTargain = false;
    private float originalYPosition;
    // Start is called before the first frame update
    void Start()
    {
        soundOnClick = GameObject.FindGameObjectWithTag("SoundOnClick").GetComponent<SoundOnClick>();
        originalYPosition = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(keyhit)
        {
            soundOnClick.PlayClick();
            keyCanBeHitTargain = true;
            keyhit = false;
            transform.position += new Vector3(0, -0.03f, 0);
        }
        if(transform.position.y < originalYPosition)
        {
            transform.position += new Vector3(0, 0.005f, 0);
        }
        else
        {
            keyCanBeHitTargain = true;
        }
    }
}
