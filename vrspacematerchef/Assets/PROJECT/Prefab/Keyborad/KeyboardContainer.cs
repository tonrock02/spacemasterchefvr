using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
public class KeyboardContainer : MonoBehaviour
{
    public static KeyboardContainer instance;
    public TextMeshProUGUI _text;
    public GameObject _learderBoard;
    public List<KeyDetector> _listkeyDetector = new List<KeyDetector>();
    public string _url_postscore;
    // Start is called before the first frame update
    void Start()
    {
        if (instance==null){
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        foreach (KeyDetector item in _listkeyDetector)
        {
            item.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Enter()
    {
        if (_text.text == "")
            return;

        //gameObject.SetActive(false);
        transform.position += new Vector3(0, -1000, 0);
        StartCoroutine(PostScore(_text.text, Station.instance._sumScore));
        
        foreach (KeyDetector item in _listkeyDetector)
        {
            item.gameObject.SetActive(false);
        }
    }

    public IEnumerator PostScore(string name, float score)
    {
        //https://us-central1-test-87c31.cloudfunctions.net/postscore
        string url = _url_postscore+"?"+"name="+name+"&score="+score.ToString();
        //WWWForm form = new WWWForm();
        //form.AddField("Score", score.ToString());
        //form.AddField("Name", name);
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        //Debug.Log("Finish Post Score" + www.result);
        _learderBoard.gameObject.SetActive(true);
        ResultContainer._instance.ResultScore(Station.instance._sumScore, _text.text);
    }
}
